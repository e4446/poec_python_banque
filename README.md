# Introduction et objectifs
Le programme fonctionne mal pour plusieurs raisons:
	- non maîtrise des fonctions
	- non maîtrise des classes
 	- non maîtrise des méthodes
	- maîtrise partielle des boucles

J’ai donc essayé de faire le maximum avec des if, elif et else.

J’ai volontairement intégré des chiffres (1,2,3,4 etc.) dans les print pour identifier où je me trouve dans le programme quand il s’exécute. 
L’objectif était de les enlever à la fin mais comme le programme fonctionne mal, je les ai laissé pour faciliter la relecture/debug.

Le programme est "main.py".

## Travaux sur le compte courant
Ce qui fonctionne :
Les versements, retraits et agios fonctionnent tant que le résultat des différentes opérations n’est pas égal à 0.
Le découvert autorisé inclut le montant des agios. Pour un montant de départ de 0€, le montant de retrait max est donc de -909 €.

Ce qui ne fonctionne pas:
Quand le résultat d’une opération est égal à 0, cela bug.
Pas de gestion des erreurs.

## Travaux sur le compte épargne
Ce qui fonctionne :
Les versements, et retraits fonctionnent tant que le résultat des opérations n’est pas égal à 0.

Ce qui ne fonctionne pas :
Quand le résultat d’une opération est égal à 0, cela bug.
Pas de gestion des erreurs.

## Observations générales
Je ne peux pas switcher librement du compte courant au compte épargne et inversement.
Pas de sauvegarde.
Pour essayer d'intégrer class et méthodes à l'exercice, j'ai fait un test sur une classe Compte dans le fichier "test class Compte.py". Comme il n'est pas abouti, il n'est pas utilisé par le programme "main.py".