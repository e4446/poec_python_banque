# Début génération du numéro de compte et du nom client

import random

numéro_compte=(random.random()*100000)
numéro_compte2=int(numéro_compte)

nom_proprietaire=input("veuillez entrer votre nom :")

print("votre nom est",nom_proprietaire)
print("votre numéro de compte est :", numéro_compte2)

# Définition des variables générales
autorisation_decouvert = -1000
taux_agios = 10
taux_interet = 5

# Choix du compte
choix_du_compte=input("Voulez-vous accéder à votre compte courant ou à votre compte épargne ? Taper Courant ou Epargne")

# Première opération sur le compte courant pour créer la variable "nouveau_solde_compte_courant"
if choix_du_compte == "Courant":
    solde_initial_compte_courant = 0
    solde_initial_compte_courant2 = int(solde_initial_compte_courant)
    print("1 Votre solde initial est de :", solde_initial_compte_courant2, "€")
    versement_compte_courant = input("1 Quel montant souhaitez-vous déposer sur le compte courant ?")
    versement_compte_courant2 = float(versement_compte_courant)

    if versement_compte_courant2 > 0:
        nouveau_solde_compte_courant : float = solde_initial_compte_courant2 + versement_compte_courant2
        print("1 Votre versement à été pris en compte, votre nouveau solde est de :", nouveau_solde_compte_courant, "€")
        solde_initial_compte_courant2 = nouveau_solde_compte_courant

    elif versement_compte_courant2 < 0:
        message_agios = input("1 Votre solde est inférieur à 0, vous aller payer des agios, retirer ? Oui/Non")

        if message_agios == "Oui":
            print("1 votre autorisation de découvert est de", autorisation_decouvert, "€ avec un taux d'agios de", taux_agios,"%")
            nouveau_solde_compte_courant = (solde_initial_compte_courant2 + versement_compte_courant2) * (1 + (taux_agios / 100))
            if nouveau_solde_compte_courant < autorisation_decouvert:
                nouveau_solde_compte_courant = versement_compte_courant2 - (nouveau_solde_compte_courant / (1 + (taux_agios / 100)))
                print("1 Vous ne pouvez pas excéder", autorisation_decouvert,"€", "veuillez renouveler votre saisie")
                print("1 Votre solde est de",round(nouveau_solde_compte_courant, 2), "€")
            else:
                print("1 Votre nouveau solde est de", round(nouveau_solde_compte_courant, 2), "€")
                solde_initial_compte_courant2 = nouveau_solde_compte_courant
    elif versement_compte_courant2 ==0:
        nouveau_solde_compte_courant = solde_initial_compte_courant2 + versement_compte_courant2
        print("1 Vous avez déposez 0€, votre solde n'a pas été modifié")
        solde_initial_compte_courant2 = nouveau_solde_compte_courant

nouvelle_operation = input("1 Souhaitez-vous effectuer une opération sur le compte courant? Oui/Non")

# Boucle pour d'autres opérations sur le compte courant, on se sert de la variable "nouveau_solde_compte_courant"
if nouvelle_operation == "Oui":
    for nouveau_versement in choix_du_compte:
        versement_compte_courant = input("2 Quel montant souhaitez-vous déposer sur le compte courant ?")
        versement_compte_courant2 = float(versement_compte_courant)
        if nouveau_solde_compte_courant+versement_compte_courant2 > 0:
            nouveau_solde_compte_courant = solde_initial_compte_courant2 + versement_compte_courant2
            print("2 Versement OK, votre nouveau solde est de :", round(nouveau_solde_compte_courant, 2), "€")
            solde_initial_compte_courant2 = nouveau_solde_compte_courant
            nouvelle_operation=input("2 Souhaitez vous effectuer une autre opération ? Oui/Non")
            if nouvelle_operation == "Non":
                break

        elif nouveau_solde_compte_courant + versement_compte_courant2 < 0:
            message_agios = input("2 Votre solde sera inférieur à 0, vous allez payer des agios? Oui/Non")

            if message_agios == "Oui":
                print("2 votre autorisation de découvert est de", autorisation_decouvert, "€ avec un taux d'agios de",taux_agios,"%")
                nouveau_solde_compte_courant = (solde_initial_compte_courant2 + versement_compte_courant2) * (1 + (taux_agios / 100))
                if nouveau_solde_compte_courant < autorisation_decouvert:
                    nouveau_solde_compte_courant = versement_compte_courant2 - (nouveau_solde_compte_courant / (1 + (taux_agios / 100)))
                    print("2 Vous ne pouvez pas excéder", autorisation_decouvert,"€", "veuillez renouveler votre saisie")
                    print("2 Votre solde est de",round(nouveau_solde_compte_courant, 2), "€")
                else:
                    print("2 Votre nouveau solde est de", round(nouveau_solde_compte_courant, 2), "€")
                    solde_initial_compte_courant2 = nouveau_solde_compte_courant

                nouvelle_operation = input("2 Souhaitez vous effectuer une autre opération ? Oui/Non")
                if nouvelle_operation == "Non":
                    print("Fin des operations sur le compte courant")

            if message_agios == "Non":
                nouvelle_operation = input("2 Souhaitez-vous effectuer une autre opération? Oui/Non")
                if nouvelle_operation == "Non":
                    break

# Fin des opérations sur le compte courant
if nouvelle_operation == "Non":
    print("3 Fin des opérations sur votre compte courant")

# Nouvelle sélection compte
choix_du_compte=input("3 Voulez-vous accédez à votre compte épargne ? Si oui, taper Epargne")

# Première opération sur le compte épargne pour créer la variable "nouveau_solde_compte_epargne"
if choix_du_compte == "Epargne":
    solde_initial_compte_epargne = 0
    solde_initial_compte_epargne2 = int(solde_initial_compte_epargne)
    print("4 Votre solde initial est de :", solde_initial_compte_epargne2, "€")
    versement_compte_epargne = input("4 Quel montant souhaitez-vous déposer sur le compte épargne ?")
    versement_compte_epargne2 = float(versement_compte_epargne)

    if versement_compte_epargne2 > 0:
            nouveau_solde_compte_epargne = (solde_initial_compte_epargne2 + versement_compte_epargne2)*(1+taux_interet / 100)
            print("4 Versement OK, tx intérêt :", taux_interet, "%, nouveau solde :", round(nouveau_solde_compte_epargne,2),"€")
            solde_initial_compte_epargne2 = nouveau_solde_compte_epargne

    elif versement_compte_epargne2 < 0:
        print("4 Votre solde ne peut pas être négatif sur le compte epargne")

# Choix nouvelle opération sur le compte épargne
nouvelle_operation = input("4 Souhaitez-vous effectuer une opération sur le compte epargne? Oui/Non")

# Boucle pour d'autres opérations sur le compte courant à l'aide de la variable "nouveau_solde_compte_epargne"
if nouvelle_operation == "Oui":
    for nouveau_versement in choix_du_compte:
        versement_compte_epargne = input("5 Quel montant souhaitez-vous déposer sur le compte epargne ?")
        versement_compte_epargne2 = float(versement_compte_epargne)

        if nouveau_solde_compte_epargne+versement_compte_epargne2 > 0:
            nouveau_solde_compte_epargne = (solde_initial_compte_epargne2 + versement_compte_epargne2)*(1+taux_interet / 100)
            print("5 Versement OK, votre nouveau solde est de :", round(nouveau_solde_compte_epargne,2), "€")
            solde_initial_compte_epargne2 = nouveau_solde_compte_epargne
            nouvelle_operation=input("5 Souhaitez vous effectuer une autre opération sur le compte epargne? Oui/Non")
            if nouvelle_operation == "Non":
                break

        if nouveau_solde_compte_epargne+versement_compte_epargne2 < 0:
            print("5 Solde négatif impossible, votre nouveau solde est de", round(nouveau_solde_compte_epargne, 2),"€")

# Fin des opérations sur le compte épargne
print("6 Fin des opérations, votre solde est de ", round(nouveau_solde_compte_epargne, 2))